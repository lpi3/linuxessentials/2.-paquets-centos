# Certification Linux Essentials (LPI) - Paquets CentOS

Bonjour et bienvenue dans ce scénario créé par **Jordan ASSOULINE** pour vous aider à maîtriser les commandes permettant de visualiser et d'installer des paquets sur une distribution CentOS/RedHat.

Pour commencer, tapez la commande suivante sur votre terminal:
`sudo yum install git -y && git clone https://gitlab.com/lpi3/linuxessentials/2.-paquets-centos.git && chmod a+x 2.-paquets-centos/check/*`

<br/>

---



# Exercice 1

Dans ce premier exercice, nous allons utiliser la commande `yum` pour installer des paquets.

Mais tout d'abord, plaçons-nous dans une situation où nous avons entendu parler d'une commande appelée `cowsay` qui affiche dans le terminal du texte en format ASCII et que nous avons envie d'essayer.

<br/>

## Essayer la commande cowsay

1. Exécutez la commande `cowsay` dans votre terminal

Si vous utilisez la commande `cowsay` directement dans votre terminal, vous obtiendrez un message indiquant 
*Command not found*.

Cela signifie que le système n’a pas trouvé la commande à laquelle vous faîtes référence. Le paquet correspondant n’est donc sans doute pas installé.

<br/>

## Recherche avec "yum search"

Vous pouvez effectuer des recherches sur les dépôts de paquets officiels en utilisant la commande : `yum search nom_paquet` ou `dnf search nom_paquet`. 

En effet, la commande **yum search** est utilisée pour rechercher des paquets et afficher les informations correspondantes aux paquets disponibles pour être installés.

Ainsi la commande `yum search cowsay` effectue une recherche pour toutes les occurrences du terme "cowsay" dans les noms des paquets disponibles et dans leurs descriptions.

2. Exécutez la commande `yum search cowsay` dans votre terminal

<br/>

---



# Exercice 2

La recherche a permis d’identifier, entre autres, un paquet appelé cowsay qui correspond à la commande manquante que nous avons essayée précédemment. 

<br/>

## Point sur l'utilisation de sudo

L’installation ou la désinstallation d’un paquet nécessitent les permissions particulières de l’administrateur du système appelé **root**. 

Sur la plupart des systèmes, les utilisateurs lambdas, peuvent demander temporairement les droits administrateurs grâce à la commande `sudo` (le système demandera d’entrer à nouveau leur mot de passe) à condition que l’administrateur en ait laissé la possibilité.

Dans notre situation, pas besoin d'utiliser cette commande, car nous sommes déjà authentifié en tant que **root**.

<br/>

## Installation d'un paquet

Pour les paquets au format **RPM**, l'installation s'effectue par la commande `yum install nom_paquet` ou `dnf install nom_paquet`.

<br/>

3. Effectuez la commande permettant d'installer le paquet **cowsay**.

<details><summary> Montrer la solution </summary>
<p>
yum install cowsay
</p>
</details>
<br/>

## Essayez la commande cowsay

Une fois le téléchargement et l'installation terminés, les fichiers vont être copiés à leurs propres localisation et la commande va devenir disponible.

Essayez donc : `cowsay Vive Linux!`.

<br/>

## Correction

Vous pouvez vérifier que vous avez correctement effectué l'exercice en utilisant la commande suivante :
`./2.-paquets-centos/check/2.check.sh`

Si la commande vous renvoie **OK** alors c'est que l'exercice a correctement été réalisé.

<br/>

---



# Exercice 3

Dans ce troisième exercice, nous allons utiliser la commande `yum` pour désinstaller des paquets.

<br/>

## Désinstaller le paquet cowsay

Pour le moment, la commande cowsay est toujours disponible: `cowsay Hello!`

Les mêmes commandes prennent en paramètre le mot **remove** plutôt qu'**install** pour procéder à la désinstallation des paquets.

<br/>

4. Grâce à la commande `yum remove`, désinstallez le paquet : `cowsay` sur cette machine.

*Remarque: la commande sudo est aussi nécessaire pour désinstaller les paquets lorsque nous ne sommes pas authentifiés avec l'utilisateur root*

<details><summary> Montrer la solution </summary>
<p>
yum remove cowsay
</p>
</details>
<br/>

Vous pouvez vérifiez que la désinstallation s'est correctement déroulée, en essayant à nouveau la commande `cowsay Hello!`

<br/>

## Correction

Vous pouvez vérifier que vous avez correctement effectué l'exercice en utilisant la commande suivante :
`./2.-paquets-centos/check/3.check.sh`

Si la commande vous renvoie **OK** alors c'est que l'exercice a correctement été réalisé.